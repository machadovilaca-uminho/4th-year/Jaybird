<p align="center">
  <img src="logo.png" />
</p>

Public Messaging System based on distributed systems middleware (https://github.com/machadovilaca/Jaybird/tree/master/server/src/main/java/modules/distribution)

Developed within the "Distributed Systems Fundamentals" course, this module is an application abstracted middleware, and it implements concepts such as Leader Election and Two Phase Commit to ensure correctness and a causally coherent view of system-wide operations.
