package client.domains;

import java.util.List;

public class Message extends Model {
    private String username;
    private String content;
    private String date;
    private List<Subject> subjects;

    public Message(String username, String content, String date, List<Subject> subjects) {
        this.username = username;
        this.content = content;
        this.date = date;
        this.subjects = subjects;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<Subject> getSubjects() {
        return subjects;
    }

    public void setSubjects(List<Subject> subjects) {
        this.subjects = subjects;
    }

    @Override
    public String toString() {
        return "Message{" +
                "username=" + username +
                ", content='" + content + '\'' +
                ", date=" + date +
                ", subjects=" + subjects +
                '}';
    }
}
