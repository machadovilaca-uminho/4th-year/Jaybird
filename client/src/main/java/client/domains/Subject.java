package client.domains;

import java.io.Serializable;

public enum Subject implements Serializable {
    Companies,
    Science,
    Technology,
    Entertainment,
    Health,
    Sports
}
