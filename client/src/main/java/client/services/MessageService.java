package client.services;

import client.config.HostsService;
import client.domains.Message;
import kong.unirest.GenericType;
import kong.unirest.Unirest;

import java.util.List;

public class MessageService {
    public List<Message> getMessages(String username, String password) {
        return Unirest.get(HostsService.getHost() + "/messages")
                .header("username", username)
                .header("password", password)
                .asObject(new GenericType<List<Message>>(){})
                .getBody();
    }

    public void sendMessage(String username, String password, Message message) {
        Unirest.post(HostsService.getHost() + "/messages")
                .header("Content-Type", "application/json")
                .header("username", username)
                .header("password", password)
                .body(message)
                .asEmpty();
    }
}
