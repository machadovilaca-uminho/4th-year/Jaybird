package client.services;

import client.config.HostsService;
import client.domains.InvalidCredentialsException;
import client.domains.Subject;
import client.domains.User;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class UserService {
    public User getUser(String username, String password) throws InvalidCredentialsException {
        final AtomicInteger error = new AtomicInteger(0);

        HttpResponse<User> r = Unirest.get(HostsService.getHost() + "/users")
                .header("username", username)
                .header("password", password)
                .asObject(User.class)
                .ifFailure(response -> error.set(response.getStatus()));

        if (error.get() != 0) {
           throw new InvalidCredentialsException();
        }

        return r.getBody();
    }

    public User createUser(String username, String password) throws InvalidCredentialsException {
        Unirest.post(HostsService.getHost() + "/users")
                .header("Content-Type", "application/json")
                .body(new User(username, password, new ArrayList<>()))
                .asEmpty();

        return getUser(username, password);
    }

    public User subscribeSubjects(String username, String password, List<Subject> subjects) throws InvalidCredentialsException {
        Unirest.post(HostsService.getHost() + "/users/subjects")
                .header("Content-Type", "application/json")
                .header("username", username)
                .header("password", password)
                .body(subjects)
                .asEmpty();

        return getUser(username, password);
    }
}
