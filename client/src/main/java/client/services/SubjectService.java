package client.services;

import client.config.HostsService;
import client.domains.Subject;
import kong.unirest.GenericType;
import kong.unirest.Unirest;

import java.util.List;

public class SubjectService {
    public List<Subject> getSubjects() {
        return Unirest.get(HostsService.getHost() + "/subjects")
                .asObject(new GenericType<List<Subject>>(){})
                .getBody();
    }
}
