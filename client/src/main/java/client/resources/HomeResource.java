package client.resources;

import client.domains.Message;
import org.glassfish.jersey.server.mvc.Template;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/")
public class HomeResource {
    @GET
    @Template(name = "/login.ftl")
    @Produces(MediaType.TEXT_HTML)
    public String home() {
        return "Jaybird";
    }
}
