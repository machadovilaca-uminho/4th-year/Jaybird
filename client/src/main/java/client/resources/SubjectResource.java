package client.resources;

import client.domains.Subject;
import client.domains.User;
import client.services.SubjectService;
import client.services.UserService;
import org.glassfish.jersey.server.mvc.Template;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("subjects")
public class SubjectResource {
    private SubjectService subjectService = new SubjectService();

    @GET
    @Template(name = "/subjects.ftl")
    @Produces(MediaType.TEXT_HTML)
    public List<Subject> getSubjects() {
        return subjectService.getSubjects();
    }
}
