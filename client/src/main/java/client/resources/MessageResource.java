package client.resources;

import client.domains.InvalidCredentialsException;
import client.domains.Message;
import client.domains.Subject;
import client.domains.User;
import client.services.MessageService;
import client.services.SubjectService;
import org.glassfish.jersey.server.mvc.Template;
import org.glassfish.jersey.server.mvc.Viewable;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Path("messages")
public class MessageResource {
    private MessageService messageService = new MessageService();
    private SubjectService subjectService = new SubjectService();

    @GET
    @Template(name = "/messages.ftl")
    @Produces(MediaType.TEXT_HTML)
    public Response getMessages(@CookieParam("username") String username, @CookieParam("password") String password) {
        Map<String, Object> models = new HashMap<>();
        models.put("messages", messageService.getMessages(username, password));
        models.put("subjects", subjectService.getSubjects());

        return Response.ok(new Viewable("/messages.ftl", models)).build();
    }

    @POST
    @Produces(MediaType.TEXT_HTML)
    public Response sendMessage(
            @CookieParam("username") String username, @CookieParam("password") String password,
            @FormParam("content") String content, @FormParam("subjects[]") List<Subject> subjects
    ) {
        messageService.sendMessage(username, password, new Message(username, content, null, subjects));
        return getMessages(username, password);
    }
}
