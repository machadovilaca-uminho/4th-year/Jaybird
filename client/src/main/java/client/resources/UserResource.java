package client.resources;

import client.domains.InvalidCredentialsException;
import client.domains.Subject;
import client.domains.User;
import client.services.SubjectService;
import client.services.UserService;
import org.glassfish.jersey.server.mvc.Template;
import org.glassfish.jersey.server.mvc.Viewable;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Path("users")
public class UserResource {
    private UserService userService = new UserService();
    private SubjectService subjectService = new SubjectService();

    @GET
    @Produces(MediaType.TEXT_HTML)
    public Response getUser(@CookieParam("username") String username, @CookieParam("password") String password)
            throws InvalidCredentialsException {
        User user = userService.getUser(username, password);
        return answer(user);
    }

    @POST
    @Produces(MediaType.TEXT_HTML)
    public Response createUser(@FormParam("username") String username, @FormParam("password") String password)
            throws InvalidCredentialsException {
        User user;

        try {
            user = userService.getUser(username, password);
        } catch (InvalidCredentialsException e) {
            user = userService.createUser(username, password);
        }

        return answer(user);
    }

    @POST
    @Path("/subjects")
    @Produces(MediaType.TEXT_HTML)
    public Response subscribeSubjects(
            @CookieParam("username") String username, @CookieParam("password") String password,
            @FormParam("subjects[]") List<Subject> subjects)
            throws InvalidCredentialsException {
        User user = userService.subscribeSubjects(username, password, subjects);
        return answer(user);
    }

    private Response answer(User user) {
        Map<String, Object> models = new HashMap<>();
        models.put("user", user);
        models.put("subjects", subjectService.getSubjects());

        return Response.ok(new Viewable("/user.ftl", models))
                .cookie(new NewCookie("username", user.getUsername()))
                .cookie(new NewCookie("password", user.getPassword()))
                .build();
    }
}
