package client.config;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;
import org.glassfish.jersey.server.mvc.freemarker.FreemarkerMvcFeature;

public class ViewConfig extends ResourceConfig {

    public ViewConfig() {
        packages("client");
        property(ServerProperties.BV_SEND_ERROR_IN_RESPONSE, true);
        property(FreemarkerMvcFeature.TEMPLATE_BASE_PATH, "/");
        register(FreemarkerMvcFeature.class);
    }
}
