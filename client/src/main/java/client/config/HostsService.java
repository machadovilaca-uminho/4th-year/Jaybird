package client.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

public class HostsService {
    private static Logger logger = Logger.getLogger(HostsService.class.getName());
    private static String[] hosts = initHosts();

    private static Properties initProperties() {
        Properties properties = new Properties();
        try {
            InputStream is = HostsService.class.getClassLoader().getResourceAsStream("config.properties");
            assert is != null;
            properties.load(is);
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return properties;
    }

    private static String[] initHosts() {
        Properties properties = initProperties();
        return properties.getProperty("servers").split(",");
    }

    public static String getHost() {
        int rnd = new Random().nextInt(hosts.length);
        logger.log(Level.INFO,"Will connect to " + hosts[rnd]);
        return hosts[rnd];
    }
}
