<#macro page>
    <html>
        <head>
            <title>Jaybird</title>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        </head>
        <body>
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand" href="/">Jaybird</a>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active"><a class="nav-link" href="/users">User</a></li>
                        <li class="nav-item active"><a class="nav-link" href="/messages">Messages</a></li>
                        <li class="nav-item active"><a class="nav-link" href="/subjects">Subjects</a></li>
                    </ul>
                </div>
            </nav>

            <div class="p-lg-5">
                <#nested>
            </div>

            <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
            <script src="https://cdn.jsdelivr.net/npm/js-cookie@beta/dist/js.cookie.min.js"></script>

            <script>
                if ((Cookies.get('username') === undefined || Cookies.get('password') === undefined) && window.location.pathname !== "/") {
                    document.location.href="/";
                }
            </script>
        </body>
    </html>
</#macro>
