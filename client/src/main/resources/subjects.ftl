<#import "structure.ftl" as structure>

<@structure.page>
    <h3 class="pb-2">Subjects:</h3>
    <ul class="list-group">
        <#list model as subject>
            <li class="list-group-item">${subject}</li>
        </#list>
    </ul>
</@structure.page>
