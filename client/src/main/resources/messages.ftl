<#import "structure.ftl" as structure>

<@structure.page>
    <button type="button" class="btn btn-primary mb-5" data-toggle="modal" data-target="#newMessage">
        New message
    </button>

    <#if messages?size != 0>
        <#list messages as message>
            <div class="card mb-5">
                <div class="card-header">
                    <h5>${message.username}</h5>
                    [<#list message.subjects as subject>
                        ${subject}<#if subject_has_next>,</#if>
                    </#list>]
                </div>
                <div class="card-body">
                    <p class="card-text">${message.content}</p>
                </div>
                <div class="card-footer text-muted">
                    ${message.date}
                </div>
            </div>
        </#list>
    <#else>
        <h3>No Messages</h3>
    </#if>

    <div class="modal fade" id="newMessage" tabindex="-1" role="dialog" aria-labelledby="newMessage" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="newMessage">New Message</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="/messages" method="post">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="content" class="col-form-label">Content:</label>
                            <textarea class="form-control" id="content" name="content"></textarea>
                        </div>
                        <ul class="list-group">
                            <#list subjects as subject>
                                <li class="list-group-item" style="display: flex;">
                                    ${subject}
                                    <div class="form-group form-check" style="position: absolute; margin-left: 8em">
                                        <input type="checkbox" class="form-check-input" id="subjects[]" name="subjects[]" value="${subject}">
                                    </div>
                                </li>
                            </#list>
                        </ul>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Send</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</@structure.page>
