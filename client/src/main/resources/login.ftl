<#import "structure.ftl" as structure>

<@structure.page>
    <form action="/users" method="post" class="justify-content-center">
        <div class="form-group">
            <label for="username">Username</label>
            <input type="text" class="form-control" id="username" name="username" placeholder="Enter email">
        </div>
        <div class="form-group">
            <label for="password">Password</label>
            <input type="password" class="form-control" id="password" name="password" placeholder="Password">
        </div>
        <button type="submit" class="btn btn-primary">Sign In / Sign Up</button>
    </form>
</@structure.page>
