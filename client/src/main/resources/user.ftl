<#import "structure.ftl" as structure>

<@structure.page>
    <h3 class="mb-5 mr-3" style="display:inline-block">Username:</h3><span style="font-size: 24px">${user.username}</span>

    <h3 class="mb-2">Subjects: </h3>
    <form action="/users/subjects" method="post">
        <ul class="list-group">
            <#list subjects as subject>
                <li class="list-group-item" style="display: flex;">
                    ${subject}
                    <div class="form-group form-check" style="position: absolute; margin-left: 8em">
                        <input type="checkbox" class="form-check-input" id="subjects[]" name="subjects[]" value="${subject}" <#if user.subscribedSubjects?seq_contains(subject)>checked</#if>>
                    </div>
                </li>
            </#list>
        </ul>
        <button type="submit" class="btn btn-primary mt-3">Subscribe</button>
    </form>
</@structure.page>
