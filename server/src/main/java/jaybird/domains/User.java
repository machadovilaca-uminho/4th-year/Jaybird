package jaybird.domains;

import java.util.List;

public class User extends Model {
    private String username;
    private String password;
    private List<Subject> subscribedSubjects;

    public User(String username, String password, List<Subject> subscribedSubjects) {
        this.username = username;
        this.password = password;
        this.subscribedSubjects = subscribedSubjects;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Subject> getSubscribedSubjects() {
        return subscribedSubjects;
    }

    public void setSubscribedSubjects(List<Subject> subscribedSubjects) {
        this.subscribedSubjects = subscribedSubjects;
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", subscribedSubjects=" + subscribedSubjects +
                '}';
    }
}
