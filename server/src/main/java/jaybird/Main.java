package jaybird;

import jaybird.services.JaybirdImpl;
import modules.communication.Host;
import modules.communication.WrongHostFormatException;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.server.ResourceConfig;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {
    public static JaybirdImpl server;
    private static Properties properties;

    public static void main(String[] args) throws WrongHostFormatException {
        if (args.length < 2) {
            throw new WrongHostFormatException("Insufficient arguments");
        }

        initProperties();
        startDistributedServer(args[0]);
        startHttpServer(args[1]);
    }

    private static void startDistributedServer(String rawHosts) throws WrongHostFormatException {
        Host myHost = new Host(rawHosts);
        List<Host> hosts = initHosts();
        File d = new File("/tmp/" + myHost.getPort());
        d.mkdirs();

        server = new JaybirdImpl(myHost, hosts, d);
        server.start();
    }

    private static void startHttpServer(String baseURI) throws WrongHostFormatException {
        if (!baseURI.matches(Host.fullRegex)) {
            throw new WrongHostFormatException(baseURI);
        } else {
            baseURI = "http://" + baseURI + "/";
        }

        try {
            final ResourceConfig resourceConfig = new ResourceConfig().packages("jaybird.services.resources");
            final HttpServer server = GrizzlyHttpServerFactory.createHttpServer(URI.create(baseURI), resourceConfig, false);
            Runtime.getRuntime().addShutdownHook(new Thread(server::shutdownNow));
            server.start();

            System.out.println(String.format("Application started.\nTry out %s\nStop the application using CTRL+C", baseURI));
            Thread.currentThread().join();
        } catch (IOException | InterruptedException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static void initProperties() {
        properties = new Properties();
        try {
            InputStream is = Main.class.getClassLoader().getResourceAsStream("config.properties");
            assert is != null;
            properties.load(is);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private static List<Host> initHosts() throws WrongHostFormatException {
        List<Host> hosts = new ArrayList<>();

        String[] servers = properties.getProperty("servers").split(",");
        for (String server : servers) {
            hosts.add(new Host(server));
        }

        return hosts;
    }
}
