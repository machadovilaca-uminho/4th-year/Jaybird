package jaybird.services;

import jaybird.domains.InvalidCredentialsException;
import jaybird.domains.Message;
import jaybird.domains.Subject;
import jaybird.domains.User;
import modules.communication.Host;
import modules.distribution.DistributedServer;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.*;

public class JaybirdImpl extends DistributedServer implements Jaybird {
    private Map<String, User> users;
    private List<Message> messages;

    public JaybirdImpl(Host myHost, List<Host> hosts, File storageDirectory) {
        super(myHost, hosts, storageDirectory);
        this.users = new HashMap<>();
        this.messages = new ArrayList<>();
    }

    public void objectReceivedCallback(Object object) {
        if (object instanceof Message) {
            messages.add((Message) object);
        } else if (object instanceof User) {
            User u = (User) object;
            users.put(u.getUsername(), u);
        }
    }

    public void createUser(String username, String password) throws InvalidCredentialsException {
        if (users.containsKey(username)) {
            throw new InvalidCredentialsException();
        }

        User user = new User(username, password, new ArrayList<>());
        this.send(user);
    }

    public User getUser(String username, String password) throws InvalidCredentialsException {
        return validateUser(username, password);
    }

    public List<Subject> getSubjects() {
        return Arrays.asList(Subject.values());
    }

    public void subscribeSubjects(String username, String password, List<Subject> subjects) throws InvalidCredentialsException {
        User user = validateUser(username, password);
        user.setSubscribedSubjects(subjects);
        this.send(user);
    }

    public void sendMessage(String username, String password, String content, List<Subject> subjects) throws InvalidCredentialsException {
        User user = validateUser(username, password);
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        this.send(new Message(user.getUsername(), content, sdfDate.format(new Date()), subjects));
    }

    public List<Message> getMessages(String username, String password) throws InvalidCredentialsException {
        List<Subject> subscribedSubjects = validateUser(username, password).getSubscribedSubjects();
        List<Message> results = new ArrayList<>();

        for (int i = messages.size() - 1; results.size() < 11 && i >= 0; i--) {
            Message m = messages.get(i);

            if (subscribedSubjects.stream().anyMatch(m.getSubjects()::contains)) {
                results.add(m);
            }
        }

        return results;
    }


    // --- PRIVATE ---


    private User validateUser(String username, String password) throws InvalidCredentialsException {
        User user = users.get(username);

        if (user == null || !user.getPassword().equals(password)) {
            throw new InvalidCredentialsException();
        }

        return user;
    }
}
