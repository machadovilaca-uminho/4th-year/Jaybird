package jaybird.services;

import jaybird.domains.InvalidCredentialsException;
import jaybird.domains.Message;
import jaybird.domains.Subject;
import jaybird.domains.User;

import java.util.List;

public interface Jaybird {
    void createUser(String username, String password) throws InvalidCredentialsException;

    User getUser(String username, String password) throws InvalidCredentialsException;

    List<Subject> getSubjects();

    void subscribeSubjects(String username, String password, List<Subject> subjects) throws InvalidCredentialsException;

    void sendMessage(String username, String password, String content, List<Subject> subjects) throws InvalidCredentialsException;

    List<Message> getMessages(String username, String password) throws InvalidCredentialsException;
}
