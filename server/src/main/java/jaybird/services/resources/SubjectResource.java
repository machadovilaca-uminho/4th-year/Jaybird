package jaybird.services.resources;

import jaybird.Main;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("subjects")
public class SubjectResource {
    @GET
    @Produces("application/json")
    public Response getSubjects() {
        return Response.ok(Main.server.getSubjects(), MediaType.APPLICATION_JSON).build();
    }
}
