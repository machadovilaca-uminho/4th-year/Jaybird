package jaybird.services.resources;

import jaybird.Main;
import jaybird.domains.InvalidCredentialsException;
import jaybird.domains.Message;
import jaybird.domains.Subject;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Path("messages")
public class MessageResource {
    @GET
    @Produces("application/json")
    public Response getMessages(@HeaderParam("username") String username, @HeaderParam("password") String password) {
        try {
            List<Message> messages = Main.server.getMessages(username, password);
            return Response.ok(messages, MediaType.APPLICATION_JSON).build();
        } catch (InvalidCredentialsException invalidCredentialsException) {
            return Response.status(401).build();
        }
    }

    @POST
    public Response sendMessage(@HeaderParam("username") String username,
                                @HeaderParam("password") String password,
                                String jsonRequest) {
        JSONObject requestedJSON = new JSONObject(jsonRequest);
        String content = requestedJSON.getString("content");
        JSONArray rawSubjects = requestedJSON.getJSONArray("subjects");

        List<Subject> subjects = new ArrayList<>();
        rawSubjects.iterator().forEachRemaining(subject -> subjects.add(Subject.valueOf((String) subject)));

        try {
            Main.server.sendMessage(username, password, content, subjects);
            return Response.ok().build();
        } catch (InvalidCredentialsException invalidCredentialsException) {
            return Response.status(401).build();
        }
    }
}
