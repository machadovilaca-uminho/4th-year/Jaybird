package jaybird.services.resources;

import jaybird.Main;
import jaybird.domains.InvalidCredentialsException;
import jaybird.domains.Subject;
import jaybird.domains.User;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Path("users")
public class UserResource {
    @GET
    @Produces("application/json")
    public Response getUser(@HeaderParam("username") String username, @HeaderParam("password") String password) {
        try {
            User user = Main.server.getUser(username, password);
            return Response.ok(user, MediaType.APPLICATION_JSON).build();
        } catch (InvalidCredentialsException invalidCredentialsException) {
            return Response.status(401).build();
        }
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces("application/json")
    public Response createUser(String jsonRequest) throws InvalidCredentialsException {
        JSONObject requestedJSON = new JSONObject(jsonRequest);
        String username = requestedJSON.getString("username");
        String password = requestedJSON.getString("password");

        Main.server.createUser(username, password);
        return Response.ok().build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/subjects")
    public Response subscribeSubjects(@HeaderParam("username") String username,
                                      @HeaderParam("password") String password,
                                      String jsonRequest) {
        JSONArray requestedJSON = new JSONArray(jsonRequest);

        List<Subject> subjects = new ArrayList<>();
        requestedJSON.iterator().forEachRemaining(subject -> subjects.add(Subject.valueOf((String) subject)));

        try {
            Main.server.subscribeSubjects(username, password, subjects);
            return Response.ok().build();
        } catch (InvalidCredentialsException invalidCredentialsException) {
            return Response.status(401).build();
        }
    }
}
