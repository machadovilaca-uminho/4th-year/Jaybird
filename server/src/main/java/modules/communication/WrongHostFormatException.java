package modules.communication;

public class WrongHostFormatException extends Exception {
    public WrongHostFormatException(String message) {
        super(message);
    }
}
