package modules.communication;

import java.util.Objects;

public class Host {
    public final static String hostRegex = "(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\\-]*[a-zA-Z0-9])\\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\\-]*[A-Za-z0-9])";
    public final static String portRegex = "([0-9]{1,4}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])";
    public final static String fullRegex = hostRegex + ":" + portRegex;
    private String host;
    private int port;

    public Host(String host, int port) throws WrongHostFormatException {
        new Host(host + ":" + port);
    }

    public Host(String host) throws WrongHostFormatException {
        if (!host.matches(fullRegex)) {
            throw new WrongHostFormatException(host);
        }

        String[] p = host.split(":");

        this.host = p[0];
        this.port = Integer.parseInt(p[1]);
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Host host1 = (Host) o;
        return port == host1.port &&
                Objects.equals(host, host1.host);
    }

    @Override
    public int hashCode() {
        return Objects.hash(host, port);
    }

    public String format() {
        return host + ":" + port;
    }

    @Override
    public String toString() {
        return "Host{" +
                "host='" + host + '\'' +
                ", port=" + port +
                '}';
    }
}
