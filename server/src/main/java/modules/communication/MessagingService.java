package modules.communication;

public interface MessagingService {
    void addHost(Host host);

    void start();

    void stop();

    void sendAll(String input);

    void sendBroadcast(String input);

    void sendTo(Host host, String input);

    String take();

    String peek();
}
