package modules.communication;

import io.atomix.cluster.messaging.ManagedMessagingService;
import io.atomix.cluster.messaging.MessagingConfig;
import io.atomix.cluster.messaging.impl.NettyMessagingService;
import io.atomix.utils.net.Address;
import io.atomix.utils.serializer.Serializer;
import io.atomix.utils.serializer.SerializerBuilder;
import org.apache.commons.collections.Buffer;
import org.apache.commons.collections.buffer.BlockingBuffer;
import org.apache.commons.collections.buffer.BoundedFifoBuffer;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

public class MessagingServiceImpl implements MessagingService {
    private ManagedMessagingService ms;
    private Serializer s;
    private ScheduledExecutorService e;

    private Host myHost;
    private List<Host> hosts;
    private Buffer messageQueue;

    public MessagingServiceImpl(Host myHost) {
        this.myHost = myHost;

        this.ms = new NettyMessagingService("test", Address.from(myHost.getPort()), new MessagingConfig());
        this.e = Executors.newSingleThreadScheduledExecutor();
        this.s = new SerializerBuilder().addType(Message.class).build();

        this.hosts = new ArrayList<>();
        this.messageQueue = BlockingBuffer.decorate(new BoundedFifoBuffer(1000));
    }

    public void addHost(Host host) {
        hosts.add(host);
    }

    public void start() {
        ms.start();

        ms.registerHandler("message", (a, b) -> {
            Message m = s.decode(b);
            messageQueue.add(m);
        }, e);
    }

    public void stop() {
        ms.stop();
    }

    public void sendAll(String input) {
        sendExcept(input, false);
    }

    public void sendBroadcast(String input) {
        sendExcept(input, true);
    }

    public void sendExcept(String input, boolean sendToMe) {
        Message message = new Message(input);
        hosts.forEach(host -> {
            if (!host.equals(myHost) || sendToMe) {
                send(host, message);
            }
        });
    }

    public void sendTo(Host host, String input) {
        Message message = new Message(input);
        send(host, message);
    }

    private void send(Host host, Message message) {
        ms.sendAsync(
                Address.from(host.getHost(), host.getPort()),
                "message", s.encode(message)
        );
    }

    public String take() {
        return ((Message) messageQueue.remove()).getContent();
    }

    public String peek() {
        return ((Message) messageQueue.get()).getContent();
    }
}
