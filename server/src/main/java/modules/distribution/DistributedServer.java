package modules.distribution;

import modules.communication.Host;
import modules.distribution.services.ServerMessaging;
import org.apache.commons.lang3.SerializationUtils;

import java.io.File;
import java.io.Serializable;
import java.util.List;

public abstract class DistributedServer {
    private ServerMessaging sa;

    public DistributedServer(Host myHost, List<Host> hosts, File storageLocation) {
        this.sa = new ServerMessaging(this, myHost, hosts, storageLocation);
    }

    public void start() {
        sa.start();
    }

    public Object send(Serializable data) {
        byte[] answer = sa.send(SerializationUtils.serialize(data));
        return answer == null ? null : SerializationUtils.deserialize(answer);
    }

    public abstract void objectReceivedCallback(Object object);
}
