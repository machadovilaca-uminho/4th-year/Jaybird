package modules.distribution.services;

import modules.communication.Host;
import modules.communication.MessagingService;
import modules.communication.MessagingServiceImpl;
import modules.distribution.DistributedServer;
import modules.distribution.services.CommitMessageAction.CommitMessage;
import modules.distribution.services.Helper.Blocker;
import modules.distribution.services.LeaderDutiesAction.LeaderDuties;
import modules.distribution.services.LeaderElectionAction.LeaderElection;
import modules.distribution.services.UpdateStateAction.UpdateState;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.SerializationUtils;

import java.io.File;
import java.io.IOException;
import java.util.Base64;
import java.util.List;

public class ServerMessaging {
    private DistributedServer distributedServer;
    private List<Host> hosts;
    private ServerOptions serverOptions;
    private MessagingService messagingService;
    private File storageLocation;
    private Blocker blocker;
    private byte[] answer;

    private LeaderElection leaderElection;
    private CommitMessage commitMessage;
    private UpdateState updateState;

    public ServerMessaging(DistributedServer distributedServer, Host myHost, List<Host> hosts, File storageLocation) {
        this.distributedServer = distributedServer;
        this.hosts = hosts;
        this.serverOptions = new ServerOptions(myHost);
        this.storageLocation = storageLocation;
        this.blocker = new Blocker();

        startMessagingService(myHost);

        this.leaderElection = new LeaderElection(serverOptions, messagingService, this);
        this.commitMessage = new CommitMessage(serverOptions, messagingService, this, storageLocation);
        this.updateState = new UpdateState(serverOptions, messagingService, this, storageLocation);
    }

    public void start() {
        try {
            FileUtils.deleteDirectory(storageLocation);
            storageLocation.mkdirs();
        } catch (IOException e) {
            e.printStackTrace();
        }

        leaderElection.run();
        updateState.run();
    }

    public byte[] send(byte[] data) {
        blocker.start();
        commitMessage.setMessage(Base64.getEncoder().encodeToString(data));
        commitMessage.run();
        blocker.await();

        String a = new String(answer);

        if (a.equals("abort")) {
            return null;
        } else if (a.equals("timedOut")) {
            leaderElection.run();
            return null;
        }

        return answer;
    }

    public void startLeaderDuties() {
        new LeaderDuties(serverOptions, messagingService, this, hosts, storageLocation);
    }

    public void callbackResponse(byte[] answer) {
        this.answer = Base64.getDecoder().decode(answer);
        blocker.finish();
    }

    public void callbackGotMessage(byte[] answer) {
        distributedServer.objectReceivedCallback(
                SerializationUtils.deserialize(Base64.getDecoder().decode(answer)));
    }

    // --------------- PRIVATE ---------------

    private void startMessagingService(Host myHost) {
        messagingService = new MessagingServiceImpl(myHost);
        hosts.forEach(messagingService::addHost);
        messagingService.start();
    }
}
