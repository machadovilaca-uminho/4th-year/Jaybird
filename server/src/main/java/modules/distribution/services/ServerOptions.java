package modules.distribution.services;

import modules.communication.Host;
import modules.distribution.services.LeaderElectionAction.Leader;

public class ServerOptions {
    private Leader leader;
    private Host myHost;

    public ServerOptions(Host myHost) {
        this.leader = new Leader(null);
        this.myHost = myHost;
    }

    public Leader getLeader() {
        return leader;
    }

    public void setLeader(Leader leader) {
        this.leader = leader;
    }

    public Host getMyHost() {
        return myHost;
    }
}
