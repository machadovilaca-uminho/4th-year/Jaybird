package modules.distribution.services.UpdateStateAction;

import modules.communication.MessagingService;
import modules.distribution.services.ActionHandler.Handler;

class Receiver extends Handler {
    private static String messagePrefix = "UpdateState#";
    private UpdateState updateState;

    Receiver(MessagingService ms, UpdateState updateState) {
        super(ms, messagePrefix);
        this.updateState = updateState;
    }

    protected void parseMessage(String m) {
        String[] parts = m.split("#");

        if (parts[0].equals("add")) {
            updateState.getMessage(parts[1], parts[2]);
        }
    }
}
