package modules.distribution.services.UpdateStateAction;

import modules.communication.Host;
import modules.communication.MessagingService;
import modules.distribution.services.ActionHandler.Action;
import modules.distribution.services.Helper.AnswerType;
import modules.distribution.services.ServerMessaging;
import modules.distribution.services.ServerOptions;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class UpdateState extends Action {
    private File storageLocation;

    public UpdateState(ServerOptions serverOptions, MessagingService ms, ServerMessaging serverMessaging, File storageLocation) {
        super(serverOptions, ms, serverMessaging);
        this.storageLocation = storageLocation;
        new Receiver(ms, this).start();
    }

    private static String askLeaderForState(Host host) {
        return "LeaderDuties#updateState#" + host.format();
    }

    protected void startAction() {
        Host h = serverOptions.getMyHost();
        ms.sendTo(serverOptions.getLeader().getLeaderHost(), askLeaderForState(h));
        finishAction(AnswerType.NONE, null);
    }

    protected void getMessage(String id, String content) {
        String f = makeFilename(id);

        try (PrintWriter writer = new PrintWriter(f)) {
            writer.print(content);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        serverMessaging.callbackGotMessage(content.getBytes());
    }

    private String makeFilename(String id) {
        return storageLocation.getAbsolutePath() + File.separator + id + "_commit";
    }
}
