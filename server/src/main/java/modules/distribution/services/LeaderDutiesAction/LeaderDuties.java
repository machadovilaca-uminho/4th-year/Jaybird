package modules.distribution.services.LeaderDutiesAction;

import modules.communication.Host;
import modules.communication.MessagingService;
import modules.communication.WrongHostFormatException;
import modules.distribution.services.ActionHandler.Action;
import modules.distribution.services.ActionHandler.Timeout;
import modules.distribution.services.Helper.AnswerType;
import modules.distribution.services.ServerMessaging;
import modules.distribution.services.ServerOptions;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LeaderDuties extends Action {
    private List<Host> hosts;
    private List<Host> checked;
    private int ids;
    private Timeout timeout;
    private File storageLocation;

    private String message;
    private String messageUUID;
    private String messageOrigin;

    public LeaderDuties(
            ServerOptions serverOptions, MessagingService ms, ServerMessaging serverMessaging,
            List<Host> hosts, File storageLocation
    ) {
        super(serverOptions, ms, serverMessaging);
        this.hosts = hosts;
        this.checked = new ArrayList<>(hosts.size());
        this.storageLocation = storageLocation;
        initIds();
        new Receiver(ms, this).start();
    }

    private static String confirmMessage(String uuid) {
        return "CommitMessage#confirm#" + uuid;
    }


    private static String stageMessage(int id, String message) {
        return "CommitMessage#stage#" + id + "#" + message;
    }

    private static String commitMessage(int id) {
        return "CommitMessage#commit#" + id;
    }

    private static String abortMessage(int id) {
        return "CommitMessage#abort#" + id;
    }

    private static String updateState(int id, String message) {
        return "UpdateState#add#" + id + "#" + message;
    }

    private void initIds() {
        ids = 0;

        try (Stream<Path> walk = Files.walk(storageLocation.toPath())) {
            List<String> results = walk.filter(Files::isRegularFile).map(Path::toString).collect(Collectors.toList());
            results.forEach(result -> {
                String[] parts = result.split(File.separator);
                String filename = parts[parts.length - 1];
                int newIdCandidate = Integer.parseInt(filename.split("_")[0]) + 1;

                if (newIdCandidate > ids) {
                    ids = newIdCandidate;
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setMessageUUID(String messageUUID) {
        this.messageUUID = messageUUID;
    }

    public void setMessageOrigin(String messageOrigin) {
        this.messageOrigin = messageOrigin;
    }

    public void startAction() {
        checked.clear();

        ms.sendBroadcast(stageMessage(ids, message));

        timeout = new Timeout(this);
        timeout.start();
    }

    public void okayFrom(String messageId, String host) {
        try {
            if (messageId.equals(Integer.toString(ids))) {
                checked.add(new Host(host));
            }
        } catch (WrongHostFormatException e) {
            e.printStackTrace();
        }

        if (checked.size() == hosts.size()) {
            timeout.interrupt();
            ms.sendTo(createHost(messageOrigin), confirmMessage(messageUUID));
            ms.sendBroadcast(commitMessage(ids++));
            checked.clear();
            finishAction(AnswerType.NONE, null);
        }
    }

    public void timedOut() {
        ms.sendTo(createHost(messageOrigin), confirmMessage(messageUUID));
        ms.sendBroadcast(abortMessage(ids++));
        checked.clear();
        finishAction(AnswerType.NONE, null);
    }

    public void updateState(String rawHost) {
        Host host = createHost(rawHost);

        try (Stream<Path> walk = Files.walk(storageLocation.toPath())) {
            List<String> results = walk
                    .filter(Files::isRegularFile)
                    .map(Path::toString)
                    .sorted(new StateFilesNameComparator())
                    .collect(Collectors.toList());

            for (String result : results) {
                String[] parts = result.split(File.separator);
                String[] filenameParts = parts[parts.length - 1].split("_");

                if (filenameParts[1].equals("commit")) {
                    int id = Integer.parseInt(filenameParts[0]);
                    String s = Files.readString(new File(result).toPath());
                    ms.sendTo(host, updateState(id, s));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Host createHost(String host) {
        try {
            return new Host(host);
        } catch (WrongHostFormatException e) {
            e.printStackTrace();
        }

        return null;
    }
}
