package modules.distribution.services.LeaderDutiesAction;

import modules.communication.MessagingService;
import modules.distribution.services.ActionHandler.Handler;

class Receiver extends Handler {
    private static String messagePrefix = "LeaderDuties#";
    private LeaderDuties leaderDuties;

    Receiver(MessagingService ms, LeaderDuties leaderDuties) {
        super(ms, messagePrefix);
        this.leaderDuties = leaderDuties;
    }

    protected void parseMessage(String m) {
        String[] parts = m.split("#");

        switch (parts[0]) {
            case "put":
                leaderDuties.setMessage(parts[1]);
                leaderDuties.setMessageUUID(parts[2]);
                leaderDuties.setMessageOrigin(parts[3]);
                leaderDuties.startAction();
                break;
            case "okay":
                leaderDuties.okayFrom(parts[1], parts[2]);
                break;
            case "updateState":
                leaderDuties.updateState(parts[1]);
                break;
        }
    }
}
