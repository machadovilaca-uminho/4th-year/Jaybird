package modules.distribution.services.LeaderDutiesAction;

import java.io.File;
import java.util.Comparator;

public class StateFilesNameComparator implements Comparator<String> {
    @Override
    public int compare(String a, String b) {
        String[] parts = a.split(File.separator);
        int ida = Integer.parseInt(parts[parts.length - 1].split("_")[0]);

        parts = b.split(File.separator);
        int idb = Integer.parseInt(parts[parts.length - 1].split("_")[0]);

        return ida - idb;
    }
}
