package modules.distribution.services.ActionHandler;

import modules.communication.MessagingService;

public abstract class Handler extends Thread {
    private MessagingService messagingService;
    private String messagePrefix;

    public Handler(MessagingService messagingService, String messagePrefix) {
        this.messagingService = messagingService;
        this.messagePrefix = messagePrefix;
    }

    public void run() {
        System.out.println("Listening for " + messagePrefix);

        while (true) {
            String s = messagingService.peek();

            if (s.startsWith(messagePrefix)) {
                String r = messagingService.take();
                System.out.println(r);
                parseMessage(r.replaceFirst(messagePrefix, ""));
            }
        }
    }

    protected abstract void parseMessage(String m);
}
