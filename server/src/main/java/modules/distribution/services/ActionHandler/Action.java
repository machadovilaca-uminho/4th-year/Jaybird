package modules.distribution.services.ActionHandler;

import modules.communication.MessagingService;
import modules.distribution.services.Helper.AnswerType;
import modules.distribution.services.Helper.Blocker;
import modules.distribution.services.ServerMessaging;
import modules.distribution.services.ServerOptions;

public abstract class Action {
    protected ServerOptions serverOptions;
    protected MessagingService ms;
    protected ServerMessaging serverMessaging;
    private Blocker blocker;

    public Action(ServerOptions serverOptions, MessagingService ms, ServerMessaging serverMessaging) {
        this.serverOptions = serverOptions;
        this.ms = ms;
        this.serverMessaging = serverMessaging;
        this.blocker = new Blocker();
    }

    public void run() {
        blocker.start();

        startAction();

        blocker.await();
    }

    protected abstract void startAction();

    protected void finishAction(AnswerType answerType, byte[] answer) {
        if (answerType.equals(AnswerType.RESPONSE)) {
            serverMessaging.callbackResponse(answer);
            blocker.finish();
        } else if (answerType.equals(AnswerType.GOT_MESSAGE)) {
            serverMessaging.callbackGotMessage(answer);
        } else if (answerType.equals(AnswerType.NONE)) {
            blocker.finish();
        }
    }

    protected void timedOut() {}
}
