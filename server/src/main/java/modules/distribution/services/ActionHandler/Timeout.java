package modules.distribution.services.ActionHandler;

public class Timeout extends Thread {
    private Action action;

    public Timeout(Action action) {
        this.action = action;
    }

    public void run() {
        try {
            Thread.sleep(5000);
            action.timedOut();
        } catch (InterruptedException ignored) {
        }
    }
}
