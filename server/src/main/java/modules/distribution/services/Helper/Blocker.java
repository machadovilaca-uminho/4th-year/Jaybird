package modules.distribution.services.Helper;

import java.util.concurrent.Semaphore;

public class Blocker {
    private Semaphore completed;

    public Blocker() {
        this.completed = new Semaphore(1);
    }

    public void start() {
        try {
            completed.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void await() {
        try {
            completed.acquire();
            completed.release();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void finish() {
        completed.release();
    }
}
