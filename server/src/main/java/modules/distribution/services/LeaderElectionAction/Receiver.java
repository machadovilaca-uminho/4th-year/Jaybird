package modules.distribution.services.LeaderElectionAction;

import modules.communication.MessagingService;
import modules.distribution.services.ActionHandler.Handler;

class Receiver extends Handler {
    private static String messagePrefix = "LeaderElection#";
    private LeaderElection leaderElection;

    Receiver(MessagingService ms, LeaderElection leaderElection) {
        super(ms, messagePrefix);
        this.leaderElection = leaderElection;
    }

    protected void parseMessage(String m) {
        String[] parts = m.split("#");

        if (parts[0].equals("get")) {
            leaderElection.sendLeader(parts[1]);
        } else if (parts[0].equals("set")) {
            leaderElection.setLeader(parts[1]);
        }
    }
}
