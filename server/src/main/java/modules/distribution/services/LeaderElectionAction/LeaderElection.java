package modules.distribution.services.LeaderElectionAction;

import modules.communication.Host;
import modules.communication.MessagingService;
import modules.communication.WrongHostFormatException;
import modules.distribution.services.ActionHandler.Action;
import modules.distribution.services.Helper.AnswerType;
import modules.distribution.services.ServerMessaging;
import modules.distribution.services.ServerOptions;

public class LeaderElection extends Action {
    private ServerMessaging serverMessaging;
    private boolean leaderUnknown;

    public LeaderElection(ServerOptions serverOptions, MessagingService ms, ServerMessaging serverMessaging) {
        super(serverOptions, ms, serverMessaging);
        this.serverMessaging = serverMessaging;
        this.leaderUnknown = true;
        new Receiver(ms, this).start();
    }

    private static String getLeader(Host host) {
        return "LeaderElection#get#" + (host != null ? host.format() : "null");
    }

    private static String setLeader(Host host) {
        return "LeaderElection#set#" + (host != null ? host.format() : "null");
    }

    protected void startAction() {
        if(leaderUnknown) {
            Host h = serverOptions.getMyHost();
            ms.sendAll(getLeader(h));
        } else {
            startLeaderElection();
        }
    }

    // --------------- PRIVATE ---------------

    public void setLeader(String host) {
        if (host.equals("null")) {
            startLeaderElection();
        } else {
            serverOptions.setLeader(new Leader(createHost(host)));
            leaderUnknown = false;

            if (host.equals(serverOptions.getMyHost().format())) {
                serverMessaging.startLeaderDuties();
            }

            finishAction(AnswerType.NONE, null);
        }
    }

    public void sendLeader(String host) {
        Host h = serverOptions.getLeader().getLeaderHost();
        ms.sendTo(
                createHost(host),
                setLeader(h)
        );
    }

    private void startLeaderElection() {
        serverOptions.setLeader(new Leader(serverOptions.getMyHost()));
        serverMessaging.startLeaderDuties();
        leaderUnknown = false;
        finishAction(AnswerType.NONE, null);
        ms.sendAll(setLeader(serverOptions.getMyHost()));
    }

    private Host createHost(String host) {
        try {
            return new Host(host);
        } catch (WrongHostFormatException e) {
            e.printStackTrace();
        }

        return null;
    }
}
