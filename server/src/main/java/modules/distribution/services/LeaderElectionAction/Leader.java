package modules.distribution.services.LeaderElectionAction;

import modules.communication.Host;

public class Leader {
    private Host leaderHost;

    public Leader(Host leaderHost) {
        this.leaderHost = leaderHost;
    }

    public boolean exists() {
        return leaderHost != null;
    }

    public Host getLeaderHost() {
        return leaderHost;
    }

    public void setLeaderHost(Host leaderHost) {
        this.leaderHost = leaderHost;
    }

    @Override
    public String toString() {
        return "Leader{" +
                "leaderHost=" + leaderHost +
                '}';
    }
}
