package modules.distribution.services.CommitMessageAction;

import modules.communication.MessagingService;
import modules.distribution.services.ActionHandler.Handler;

class Receiver extends Handler {
    private static String messagePrefix = "CommitMessage#";
    private CommitMessage commitMessage;

    Receiver(MessagingService ms, CommitMessage commitMessage) {
        super(ms, messagePrefix);
        this.commitMessage = commitMessage;
    }

    protected void parseMessage(String m) {
        String[] parts = m.split("#");

        switch (parts[0]) {
            case "confirm":
                commitMessage.confirm(parts[1]);
                break;
            case "stage":
                commitMessage.stage(parts[1], parts[2]);
                break;
            case "commit":
                commitMessage.commit(m.split("#")[1]);
                break;
            case "abort":
                commitMessage.abort(m.split("#")[1]);
                break;
        }
    }
}
