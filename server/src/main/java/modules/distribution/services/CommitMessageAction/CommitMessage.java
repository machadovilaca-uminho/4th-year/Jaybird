package modules.distribution.services.CommitMessageAction;

import modules.communication.MessagingService;
import modules.distribution.services.ActionHandler.Action;
import modules.distribution.services.ActionHandler.Timeout;
import modules.distribution.services.Helper.AnswerType;
import modules.distribution.services.ServerMessaging;
import modules.distribution.services.ServerOptions;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.util.Base64;
import java.util.UUID;

public class CommitMessage extends Action {
    private String message;
    private File storageLocation;
    private Timeout timeout;
    private UUID uuid;

    public CommitMessage(ServerOptions serverOptions, MessagingService ms,
                         ServerMessaging serverMessaging, File storageLocation) {
        super(serverOptions, ms, serverMessaging);
        this.storageLocation = storageLocation;
        new Receiver(ms, this).start();
    }

    private static String sendMessageToLeader(String message, UUID uuid, String host) {
        return "LeaderDuties#put#" + message + "#" + uuid + "#" + host;
    }

    private static String sendOkayToLeader(String id, String host) {
        return "LeaderDuties#okay#" + id + "#" + host;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    protected void startAction() {
        uuid = UUID.randomUUID();

        ms.sendTo(serverOptions.getLeader().getLeaderHost(), sendMessageToLeader(message, uuid, serverOptions.getMyHost().format()));

        timeout = new Timeout(this);
        timeout.start();
    }

    protected void confirm(String messageUUID) {
        if (uuid.compareTo(UUID.fromString(messageUUID)) == 0) {
            timeout.interrupt();
        }
    }

    protected void timedOut() {
        finishAction(AnswerType.RESPONSE, Base64.getEncoder().encode("timedOut".getBytes()));
    }

    protected void stage(String id, String data) {
        String f = makeFilename(id, "stage");

        try (PrintWriter writer = new PrintWriter(f)) {
            writer.print(data);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        ms.sendTo(serverOptions.getLeader().getLeaderHost(), sendOkayToLeader(id, this.serverOptions.getMyHost().format()));
    }

    protected void commit(String id) {
        File init = new File(makeFilename(id, "stage"));
        File end = new File(makeFilename(id, "commit"));
        init.renameTo(end);

        try {
            byte[] data = Files.readAllBytes(end.toPath());
            finishAction(AnswerType.RESPONSE, data);
            finishAction(AnswerType.GOT_MESSAGE, data);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected void abort(String id) {
        File init = new File(makeFilename(id, "stage"));
        File end = new File(makeFilename(id, "abort"));
        init.renameTo(end);
        finishAction(AnswerType.RESPONSE, Base64.getEncoder().encode("aborted".getBytes()));
    }

    private String makeFilename(String id, String type) {
        return storageLocation.getAbsolutePath() + File.separator + id + "_" + type;
    }
}
